console.log("Hello 211!");

	/*
		so far we have explored storing values on individual variables. However, modelling real-world information is a bit more complicated than that.
	*/

	/*
		An array in programming is simply a list of data.
	*/
	let studentNumberA = "2020-1923";
	let studentNumberB = "2020-1924";
	let studentNumberC = "2020-1925";
	let studentNumberD = "2020-1926";
	let studentNumberE = "2020-1927";

	let studerNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];


//Arrays
	/*
		- Arrays are used to store multiple values in a single variable.
		- They are declared using square brackets[] also know as "Array Literals"

		Syntax:
			let arrayName = [elementA, elementB, elementC,...];
	*/


//Common Examples of Arrays
	let grade = [98.5, 94.3, 89.2, 90.1];
	let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];
	console.log(grade);
	console.log(computerBrands);

	//Possible use of an array but it is nor recommended.
	let mixedArr = [12, "Asus", null, undefined, {}];
	console.log(mixedArr);

	//Alternative way to write arrays
	let myTasks = [
		"drink html",
		"eat javascript",
		"inhale css",
		"bake sass"
	];



	//Mini Activity
	let task = ["eat", "drink", "sleep", "bath", "read"];
	let capitalCities = ["Manila", "Soul", "Tokyo", "Pyongyang"];
	console.log(task);
	console.log(capitalCities);


	//We can also store array data in variable
	let city1 = "Tokyo";
	let city2 = "Manila";
	let city3 = "Jakarta";

	let cities = [city1, city2, city3];
	console.log(cities);



//Length property
	/*
		- The  .length  property allows us to get and set the total number of items in an array.
		- length property can also be used in strings.
		- Some array method and properties can also be used with strings.
	*/
	console.log(myTasks.length);	//4
	console.log(cities.length);	//3

	//what will happen if we have a blank array?
	let blank = [];
	console.log(blank.length);	//0

	//refresher from string.length
	let fullName = "Jamie Noble";
	console.log(fullName.length);



//pag bawas ng item sa huli ng array.
	//length property can also set the total number of items in an array. Meaning we can actually DELETE the last item in the array or shorten the array by simply updating the length property of an array.
	myTasks.length = myTasks.length -1;
	console.log(myTasks.length);
	console.log(myTasks);

	//another example using decrementation
	cities.length --;
	console.log(cities);

	//applying pag-babawas sa string.
	fullName.length = fullName.length -1;
	console.log(fullName.length);
	//walang mag babago sa string. hindi applicable ang .length -1;



//Pag-dagdag sa array
	let theBeatles = ["Paul", "John", "Ringo", "George"];
	// theBeatles.length++;
	theBeatles[4] = "Cardo";
	theBeatles[theBeatles.length] = "Ely";
	theBeatles[theBeatles.length] = "Chito";
	theBeatles[theBeatles.length] = "MJ";
	console.log(theBeatles);



	//Mini Activity
	/*
		-Create a function called addTrainers that can recieve a single argument
		-add the argument in the end of theTrainers array
		-invoke and add an argument to be passed in the function
		-log theTrainer's array in the console
		-send a screenshot of your console in the batch hangouts.
	*/
	let theTrainers = ["Ash"];
	function addTrainer(add){
		theTrainers[theTrainers.length] = add;
	}
	addTrainer("Brock");
	addTrainer("Misty");
	console.log(theTrainers);



//Reading From Arrays
	/*
		- Accessing array elements is one of the more common tasks that we do with an array.
		- this can be done through the use of array indexes.
		- Each element in an array is associated with its own index or number.
		- The reason an array starts with 0 is due to how the language is designed.

		Syntax:
			arrayName[index];
	*/
	console.log(grade[0]);	//98.5
	console.log(computerBrands[3]);	//Neo

	//ganito mangyayare kapag nag access ka ng element na wala sa array. Undefined lalabas.
	console.log(grade[20]);	//undefined

	let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
	console.log(lakersLegends[1]);	//Shaq
	console.log(lakersLegends[3]);	//Magic

	//ganito mag store ng item sa variable
	let currentLaker = lakersLegends[2];
	console.log(currentLaker);	//Lebron

	//Re-assigning items in array
	console.log(lakersLegends);
	lakersLegends[2] = "Paul Gasol"
	console.log(lakersLegends);



	//Mini Activity
	/*
		create a function named findBlackMamba which is able to recieve an index number as a single argument and return the item accessed by its index.
		-function should be able to recieve one argument.
		-return the blackMamba accessed by the index.
		-create a variable outside the function called blackMamba and store the value returned by the function in it.
		-log the blackMamba variable in the console.
	*/
	function findBlackMamba(num){
		return lakersLegends[num];
	};
	let blackMamba = findBlackMamba(0);
	console.log(blackMamba);



	//Mini Activity
	/*
		update and reassign the last two items in the array with your own favorite food.
		log the updated array in your console.
		send a screenshot to the bacth hangouts.
	*/
	let favoriteFoods = [
		"Tonkatsu",
		"Adobo",
		"Humberger",
		"Sinigang",
		"Pizza"
	];
	favoriteFoods[3] = "Kare-Kare";
	favoriteFoods[4] = "Itlog";
	console.log(favoriteFoods);




//Accessing the last element of an Array
	let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

	let lastElementIndex = bullsLegends.length-1;
	console.log(lastElementIndex);	//4
	console.log(bullsLegends.length);	//5
	console.log(bullsLegends[lastElementIndex]);	"Kukoc";
	console.log(bullsLegends[bullsLegends.length-1]);




//Adding items into our array
	let newArr = [];
	console.log(newArr[0]);	//undefined

	newArr[0] = "Cloud Strife";
	console.log(newArr);
	
	console.log(newArr[1]);
	newArr[1] = "Tifa Lockhart";
	console.log(newArr);

	// newArr[newArr.length-1] = "Aerith Gainsborough";

	newArr[newArr.length] = "Barret Wallace";
	console.log(newArr);




//Looping over an Array
	for(let index = 0; index < newArr.length; index++){
		console.log(newArr[index]);
	}



	let number = [5, 12, 30, 46, 40];
	for(let i = 0; i < number.length; i++){
		if(number[i] % 5 === 0){
			console.log(number[i] + " is divisible by 5");
		} else {
			console.log(number[i] + " is not divisible by 5");
		}
	}





//Multidimensional Arrays
	/*
		- Multidimensional Arrays are useful for storing complex data stcutures.
		- a practical application of this is to help visualize or create real world objects.
	*/
	let chessBoard = [
		["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
		["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
		["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
		["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
		["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
		["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
		["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
		["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]
	];
	console.log(chessBoard);
	console.log(chessBoard[1][4]);
	console.log("Pawn moves to: "+chessBoard[1][5]);	//f2

	//Mini Activity
	/*
		Access a8 and h6
	*/
	console.log(chessBoard[7][0]);
	console.log(chessBoard[5][7]);